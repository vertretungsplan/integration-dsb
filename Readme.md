## DSB crawler

This crawler gets contents from <https://www.dsbmobile.de> for the vertretungsplan.io application.
It uses an API which is/ was used by the official App to get the content.

Recent versions use the work from <https://notabug.org/fynngodau/DSBDirect/wiki/mobileapi.dsbcontrol.de>
which describes the mobileapi.dsbcontrol.de API used by the official App. Thanks to [fynngodau](https://notabug.org/fynngodau)
for documenting it (and telling me about that and the fact that I used an older API version before that).

The content list (messages, list of files) is served from this application.
The content files (plan HTMLs, ...) are served directly from the server of the DSB.

Schools (and the listening port) are managed in the source code at ``src/config/input.ts``.
Access data is passend in using environment variables
to allow making this public without disclosing the access data.

If only some access data is provided, then only the schools
with access data are crawled and served.

### License

AGPL 3.0

> vertretungsplan.io dsb crawler
> Copyright (C) 2019 - 2022 Jonas Lochmann
>
> This program is free software: you can redistribute it and/or modify
> it under the terms of the GNU Affero General Public License as
> published by the Free Software Foundation, version 3 of the
> License.
>
> This program is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
> GNU Affero General Public License for more details.
>
> You should have received a copy of the GNU Affero General Public License
> along with this program.  If not, see <https://www.gnu.org/licenses/>.
