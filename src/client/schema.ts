/*
 * vertretungsplan.io dsb crawler
 * Copyright (C) 2019 - 2022 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

export interface DocumentItem {
  Id: string, /* id */
  Date: string,
  Title: string, /* title */
  Detail: string,
  Tags: string,
  ConType: 2,
  Prio: 0,
  Index: number,
  Childs: Array<{
    Id: string,
    Date: string,
    Title: string,
    Detail: string, /* url */
    Tags: '',
    ConType: 3 /*html */ | 4 /* image */ | 6 /* weather html */,
    Prio: 0,
    Index: number,
    Childs: Array<number>
    Preview: string
  }>
  Preview: string
}

export interface MessageItem {
  Id: string                // id
  Date: string              // "17.10.2019 07:52"
  Title: string             // title
  Detail: string            // content
  Tags: ''
  ConType: 5
  Prio: 0
  Index: 0
  Childs: Array<number>
  Preview: ''
}

export type DocumentsResponse = Array<DocumentItem>
export type MessagesResponse = Array<MessageItem>
