/*
 * vertretungsplan.io dsb crawler
 * Copyright (C) 2019 - 2022 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import Ajv from 'ajv'
import { readFileSync } from 'fs'
import { resolve } from 'path'
import { fileURLToPath } from 'url'
import { DocumentsResponse, MessagesResponse } from './schema.js'

const ajv = new Ajv({ schemaId: 'id' })

function loadJson(path: string) {
  return JSON.parse(readFileSync(
    resolve(fileURLToPath(import.meta.url), '..', path)
  ).toString('utf8'))
}

const documentsResponseValidator = ajv.compile(loadJson('documentsresponse.json'))
const messagesResponseValidator = ajv.compile(loadJson('messagesresponse.json'))

export function isValidDocumentsResponse (data: unknown): data is DocumentsResponse {
  const result = documentsResponseValidator(data) as boolean

  if (!result) {
    console.log(documentsResponseValidator.errors)
  }

  return result
}

export function isValidMessagesResponse (data: unknown): data is MessagesResponse {
  const result = messagesResponseValidator(data) as boolean

  if (!result) {
    console.log(messagesResponseValidator.errors)
  }

  return result
}
