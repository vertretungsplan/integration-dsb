/*
 * vertretungsplan.io dsb crawler
 * Copyright (C) 2019 - 2022 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import entitiesModule from 'html-entities'
import moment from 'moment-timezone'
import fetch from 'node-fetch'
import { stringify as buildQueryString } from 'querystring'
import { fetchOptions } from '../request.js'
import { DocumentItem, MessageItem } from './schema.js'
import { isValidDocumentsResponse, isValidMessagesResponse } from './validator.js'

const entities = new entitiesModule.AllHtmlEntities()

interface FileInfo {
  type: 'plan' | 'download'
  title: string
  id: string
  urls: Array<string>
  lastModified: number
}

interface MessageInfo {
  id: string
  title: string
  content: string
  notify: boolean
}

export async function query ({ username, password, abort }: {
  username: string
  password: string
  abort: AbortSignal
}) {
  const authToken = await fetchJson(
    'https://mobileapi.dsbcontrol.de/authid?' + buildQueryString({
      user: username,
      password: password,
      // removing these fields causes problems, but empty values are ok
      bundleid: '',
      appversion: '',
      osversion: '',
      pushId: ''
    }), abort
  )

  if (typeof authToken !== 'string' || authToken === '') {
    throw new Error('authentication failed')
  }

  const queryParameter = '?' + buildQueryString({ authid: authToken })

  const [ timetablesResponse, documentsResponse, newsResponse ] = await Promise.all([
    await fetchJson('https://mobileapi.dsbcontrol.de/dsbtimetables' + queryParameter, abort),
    await fetchJson('https://mobileapi.dsbcontrol.de/dsbdocuments' + queryParameter, abort),
    await fetchJson('https://mobileapi.dsbcontrol.de/newstab' + queryParameter, abort)
  ])

  if (!isValidDocumentsResponse(documentsResponse)) {
    throw new Error('invalid documents list')
  }

  if (!isValidDocumentsResponse(timetablesResponse)) {
    throw new Error('invalid timetable list')
  }

  if (!isValidMessagesResponse(newsResponse)) {
    throw new Error('invalid news response')
  }

  const files: Array<FileInfo> = [
    ...timetablesResponse.map((timetable) => convertDocumentItem(timetable, 'plan')),
    ...documentsResponse.map((timetable) => convertDocumentItem(timetable, 'download'))
  ]

  const messages: Array<MessageInfo> = newsResponse.map((item) => convertMessageItem(item))

  return {
    files,
    messages
  }
}

async function fetchJson(url: string, abort: AbortSignal) {
  const response = await fetch(url, {
    ...fetchOptions,
    signal: abort
  })

  if (!response.ok) {
    throw new Error('server returned ' + response.status)
  }

  const body = await response.json()

  return body
}

function convertDocumentItem (input: DocumentItem, type: 'plan' | 'download'): FileInfo {
  const urls = input.Childs.map((child) => child.Detail)
  const id = input.Id
  const title = input.Title
  const lastModifiedString = input.Date
  const lastModifiedMoment = moment.tz(lastModifiedString, 'DD.MM.YYYY HH:mm', 'de', true, 'Europe/Berlin')
  const lastModified = lastModifiedMoment.valueOf()

  return {
    urls,
    type,
    id,
    title,
    lastModified
  }
}

function convertMessageItem (input: MessageItem): MessageInfo {
  return {
    id: input.Id,
    title: entities.decode(input.Title),
    content: entities.decode(input.Detail),
    notify: true
  }
}
