/*
 * vertretungsplan.io dsb crawler
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
export interface Configuration {
  schools: Array<SchoolConfiguration>
  port: number
  proxyUrl: string
}

export interface SchoolConfiguration {
  id: string
  title: string
  userTypeField: string
  categories: Array<{
    id: string
    label: string
    passwordField: string
  } & CrawlOptions>
}

export interface CrawlOptions {
  username: string
  password: string
}

export function isSchoolConfigComplete (item: SchoolConfiguration) {
  if (item.categories.length === 0) {
    return false
  }

  for (let i = 0; i < item.categories.length; i++) {
    const category = item.categories[i]

    if ((!category.username) || (!category.password)) {
      return false
    }
  }

  return true
}
