/*
 * vertretungsplan.io dsb crawler
 * Copyright (C) 2019 - 2022 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Configuration } from './schema.js'

export const config: Configuration = {
  port: parseInt(process.env.PORT || '8080', 10),
  proxyUrl: process.env.PROXY_URL || '',
  schools: [
    {
      id: 'gs-gym-stuttgart',
      title: 'Geschwister-Scholl-Gymnasium Stuttgart',
      userTypeField: 'userType',
      categories: [
        {
          id: 'student',
          label: 'Schüler',
          username: process.env.GS_STUTTGART_STUDENT_USERNAME || '',
          password: process.env.GS_STUTTGART_STUDENT_PASSWORD || '',
          passwordField: 'studentPassword'
        }
      ]
    },
    {
      id: 'gss-bensheim',
      title: 'Geschwister-Scholl-Schule Bensheim',
      userTypeField: 'userType',
      categories: [
        {
          id: 'student',
          label: 'Schüler',
          username: process.env.GSS_BENSHEIM_STUDENT_USERNAME || '',
          password: process.env.GSS_BENSHEIM_STUDENT_PASSWORD || '',
          passwordField: 'studentPassword'
        }
      ]
    },
    {
      id: 'kbbz-saarlouis',
      title: 'KBBZ Saarlouis',
      userTypeField: 'userType',
      categories: [
        {
          id: 'student',
          label: 'Schüler',
          username: process.env.KBBZ_STUDENT_USERNAME || '',
          password: process.env.KBBZ_STUDENT_PASSWORD || '',
          passwordField: 'studentPassword'
        }
      ]
    },
    {
      id: 'morike-gym-stuttgart',
      title: 'Mörike-Gymnasium Stuttgart',
      userTypeField: 'userType',
      categories: [
        {
          id: 'student',
          label: 'Schüler',
          username: process.env.MGYM_STUTTGART_STUDENT_USERNAME || '',
          password: process.env.MGYM_STUTTGART_STUDENT_PASSWORD || '',
          passwordField: 'studentPassword'
        }
      ]
    }
  ]
}
