/*
 * vertretungsplan.io dsb crawler
 * Copyright (C) 2019 - 2022 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

export { FileParserItem } from './schema.js'

import { matchesHtmlFileSchema } from './validator.js'
import { postprocessFile } from './postprocess.js'
import { FileParserItem } from './schema.js'
import { parser } from 'posthtml-parser'
import iconv from 'iconv-lite'

export function parseFile (input: Buffer): Array<FileParserItem> {
  const data = parser(iconv.decode(input, 'ISO-8859-1'))

  if (!matchesHtmlFileSchema(data)) {
    throw new Error('input does not match expectation: ' + JSON.stringify(matchesHtmlFileSchema.errors))
  }

  return postprocessFile(data)
}
