/*
 * vertretungsplan.io dsb crawler
 * Copyright (C) 2019 - 2022 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import moment from 'moment-timezone'
import { HtmlSchema, RealPlanTable } from './htmlschema.js'
import { parseLessonString, readOptionalContent, readRequiredContent, sanitizeEmptyValues } from './utils.js'
import { FileParserItem } from './schema.js'
import { isRealPlanTable } from './validator.js'

export function postprocessFile (input: HtmlSchema): Array<FileParserItem> {
  const result: Array<FileParserItem> = []

  let dateString: string | null = null

  const content = input[0].content[1].content[0].content

  content.forEach((contentItem) => {
    if (typeof contentItem === 'string' || contentItem.tag === 'br') {
      // ignore

      return
    }

    if (contentItem.tag === 'span') {
      const type = contentItem.attrs.class
      const value = contentItem.content[0]

      if (type === 'vpfuerdatum') {
        dateString = value
      }
    } else if (contentItem.tag === 'table') {
      if (!isRealPlanTable(contentItem)) {
        // no plan => ignore

        return
      }

      if (!dateString) {
        throw new Error('missing date string for plan table')
      }

      const planMoment = moment.tz(dateString.trim(), 'dddd, D. MMMM YYYY', 'de', true, 'Europe/Berlin')

      if (!planMoment.isValid()) {
        throw new Error('invalid plan date: ' + JSON.stringify(dateString.trim()))
      }

      const date = planMoment.format('YYYY-MM-DD')

      const tableContent = parsePlanTable(contentItem)

      tableContent.forEach((item) => {
        item.lessons.forEach((lesson) => {
          result.push({
            date,
            class: item.class,
            lesson,
            subject: item.subject,
            subjectChanged: item.subjectChanged,
            teacher: item.teacher,
            teacherChanged: item.teacherChanged,
            room: item.room,
            roomChanged: item.roomChanged,
            info: item.info
          })
        })
      })

      dateString = null
    } else {
      throw new Error('illegal state')
    }
  })

  return result
}

function parsePlanTable (input: RealPlanTable) {
  const result: Array<{
    class: string
    lessons: Array<number>
    subject: string | null
    subjectChanged: boolean
    teacher: string | null
    teacherChanged: boolean
    room: string | null
    roomChanged: boolean
    info: string | null
  }> = []

  let hadHeader = false

  input.content.forEach((row) => {
    if (typeof row === 'string') {
      // empty

      return
    }

    if (row.content[1].tag === 'th') {
      // header

      if (hadHeader) {
        throw new Error('duplicate header')
      }

      hadHeader = true

      return
    } else if (row.content[1].tag === 'td') {
      // data row

      if (!hadHeader) {
        throw new Error('must have header before content')
      }

      const classCell = row.content[1]
      const lessonCell = row.content[3]
      const subjectCell = row.content[5]
      const teacherCell = row.content[7]
      const roomCell = row.content[9]
      const infoCell = row.content[11]

      const className = readRequiredContent(classCell.content)
      const lessonString = readRequiredContent(lessonCell.content)
      const lessons = parseLessonString(lessonString)
      const subject = sanitizeEmptyValues(readOptionalContent(subjectCell.content))
      const teacher = sanitizeEmptyValues(readOptionalContent(teacherCell.content))
      const room = sanitizeEmptyValues(readOptionalContent(roomCell.content))
      const info = sanitizeEmptyValues(readOptionalContent(infoCell.content))

      const subjectChanged = subjectCell.attrs.class === 'tdaktionenneu'
      const teacherChanged = teacherCell.attrs.class === 'tdaktionenneu'
      const roomChanged = roomCell.attrs.class === 'tdaktionenneu'

      result.push({
        class: className,
        lessons,
        subject,
        subjectChanged,
        teacher,
        teacherChanged,
        room,
        roomChanged,
        info
      })
    } else {
      throw new Error('illegal state')
    }
  })

  if (!hadHeader) {
    throw new Error('table had no header')
  }

  return result
}
