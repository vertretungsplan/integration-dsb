/*
 * vertretungsplan.io dsb crawler
 * Copyright (C) 2019 - 2022 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import Ajv from 'ajv'
import { readFileSync } from 'fs'
import { resolve } from 'path'
import { fileURLToPath } from 'url'
import { PreinfoTable, OtherInfoTable, RealPlanTable, HtmlSchema } from './htmlschema'

const ajv = new Ajv()

function loadJson(path: string) {
  return JSON.parse(readFileSync(
    resolve(fileURLToPath(import.meta.url), '..', path)
  ).toString('utf8'))
}

export const matchesHtmlFileSchema = ajv.compile(loadJson('htmljsonschema.json')) as ((input: unknown) => input is HtmlSchema) & {
  errors: unknown
}

export function isRealPlanTable (input: PreinfoTable | OtherInfoTable | RealPlanTable): input is RealPlanTable {
  return 'attrs' in input && typeof input.attrs === 'object' && input.attrs.class === 'tablekopf'
}
