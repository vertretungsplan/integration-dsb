/*
 * vertretungsplan.io dsb crawler
 * Copyright (C) 2019 - 2022 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

export type HtmlSpace = string

export type HtmlBr = { tag: 'br' }

export interface PreinfoTable {
  tag: 'table',
  content: Array<
    HtmlSpace | {
      tag: 'tr',
      content: [
        HtmlSpace,
        {
          tag: 'th',
          attrs: {
            class: 'thkopfabwesend'
          },
          content: [string]
        },
        HtmlSpace,
        {
          tag: 'td',
          attrs: {
            class: 'thabwesend'
          },
          content: [string]
        },
        HtmlSpace
      ]
    }
  >
}

export interface OtherInfoTable {
  tag: 'table',
  content: Array<
    HtmlSpace |
    {
      tag: 'tr',
      content: [
        HtmlSpace,
        {
          tag: 'td',
          content?: [string]
        },
        HtmlSpace
      ]
    }
  >
}

export interface ScriptBlock {
  tag: 'script',
  attrs: {
    language: 'JavaScript'
  },
  content: [string]
}

export interface RealPlanTableHeader {
  tag: 'tr',
  content: [
    HtmlSpace,
    {
      tag: 'th',
      attrs: {
        class: 'thplanklasse'
      },
      content: ['Klasse/Kurs']
    },
    HtmlSpace,
    {
      tag: 'th',
      attrs: {
        class: 'thplanstunde'
      },
      content: ['Stunde']
    },
    HtmlSpace,
    {
      tag: 'th',
      attrs: {
        class: 'thplanfach'
      },
      content: ['Fach']
    },
    HtmlSpace,
    {
      tag: 'th',
      attrs: {
        class: 'thplanlehrer'
      },
      content: ['Lehrer']
    },
    HtmlSpace,
    {
      tag: 'th',
      attrs: {
        class: 'thplanraum'
      },
      content: ['Raum']
    },
    HtmlSpace,
    {
      tag: 'th',
      attrs: {
        class: 'thplaninfo'
      },
      content: ['Info']
    },
    HtmlSpace
  ]
}

export interface RealPlanTableItemColumn {
  tag: 'td',
  attrs: {
    class: 'tdaktionen' | 'tdaktionenneu'
  },
  content?: [string]
}

export interface RealPlanTableItem {
  tag: 'tr',
  content: [
    HtmlSpace,
    RealPlanTableItemColumn,
    HtmlSpace,
    RealPlanTableItemColumn,
    HtmlSpace,
    RealPlanTableItemColumn,
    HtmlSpace,
    RealPlanTableItemColumn,
    HtmlSpace,
    RealPlanTableItemColumn,
    HtmlSpace,
    RealPlanTableItemColumn,
    HtmlSpace
  ]
}

export interface RealPlanTable {
  tag: 'table',
  attrs: {
    class: 'tablekopf',
    border: '2'
  },
  content: Array<RealPlanTableHeader | RealPlanTableItem | HtmlSpace>
}

export type HtmlSchema = [
  {
    tag: 'html',
    content: [
      {
        tag: 'head',
        // the content here is really unimportant
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        content: any
      },
      {
        tag: 'body',
        content: [
          {
            tag: 'body',
            content: Array<
              HtmlSpace |       // ignore
              HtmlBr |          // ignore
              {
                tag: 'span'
                attrs: { class: 'vpfuer' }
                content: [string] // just a label
              } |
              {
                tag: 'span'
                attrs: { class: 'vpfuerdatum' }
                content: [string] // example: 'Mittwoch, 11. September 2019 '
              } |
              {
                tag: 'span',
                attrs: { class: 'vpschulname' },
                content: [string]
              } |
              {
                tag: 'span',
                attrs: { class: 'vpdatum' },
                content: [string] // example: "10.09.2019, 14:25"
              } |
              PreinfoTable |
              {
                tag: 'span'
                attrs: { class: 'ueberschrift' }
                content: [string]
              } |
              OtherInfoTable |
              RealPlanTable
            >
          },
          ScriptBlock,
          HtmlSpace
        ]
      }
    ]
  }
]
