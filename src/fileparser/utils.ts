/*
 * vertretungsplan.io dsb crawler
 * Copyright (C) 2019 - 2022 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import lodash from 'lodash'

const { range } = lodash

const emptyValues = ['', '---']

export function readOptionalContent (input: [string] | undefined): string | null {
  if (input) {
    return input[0]
  } else {
    return null
  }
}

export function readRequiredContent (input: [string] | undefined): string {
  if (input) {
    return input[0]
  } else {
    throw new Error('missing required field')
  }
}

export function sanitizeEmptyValues (input: string | null): string | null {
  if (input === null) {
    return null
  } else if (emptyValues.indexOf(input) !== -1) {
    return null
  } else {
    return input
  }
}

function safeParseInt (input: string): number {
  const parsed = parseInt(input, 10)

  if (!Number.isSafeInteger(parsed)) {
    throw new Error('bad number: "' + input + '"')
  }

  return parsed
}

export function parseLessonString (input: string): Array<number> {
  const parts = input.split('-')

  if (parts.length === 1) {
    const a = safeParseInt(input)

    if (a < 0) {
      throw new Error('lesson out of range')
    }

    return [a]
  } else if (parts.length === 2) {
    const a = safeParseInt(parts[0])
    const b = safeParseInt(parts[1])

    if (a < 0 || a >= b || b - a > 10) {
      throw new Error('lesson out of range')
    }

    return range(a, b + 1)
  } else {
    throw new Error('bad lesson string: "' + input + '"')
  }
}
