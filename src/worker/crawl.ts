/*
 * vertretungsplan.io dsb crawler
 * Copyright (C) 2019 - 2022 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import fetch from 'node-fetch'
import { fetchOptions } from '../request.js'
import { query } from '../client/index.js'
import { CrawlOptions } from '../config/schema.js'
import { parseFile, FileParserItem } from '../fileparser/index.js'
import { getUrlInfo } from './get-url-info.js'

const weatherUrlRegex = /https:\/\/cache2\.dsbcontrol\.de\/Files\/11111111-1111-1111-1111-111111111111\/.*\/weather\.html/

export interface CrawlResponse {
  file: Array<{
    type: 'plan' | 'download'
    mimeType: string
    title: string
    id: string
    notify: boolean
    file: Array<{
      url: string
      sha512: string
      size: number
    }>
  }>
  message: Array<{
    id: string
    title: string
    content: string
    notify: boolean
  }>
  plan: Array<{
    date: string
    class: string
    lesson: number
    subject: string | null
    subjectChanged: boolean
    teacher: string | null
    teacherChanged: boolean
    room: string | null
    roomChanged: boolean
    info: string | null
  }>
}

export async function crawlDsb ({
  username, password, abort
}: CrawlOptions & { abort: AbortSignal }): Promise<CrawlResponse> {
  const result = await query({ username, password, abort })
  let plan: Array<FileParserItem> = []

  const timetableItems = result.files.filter((item) => item.type === 'plan')

  if (timetableItems.length === 1 && timetableItems[0].urls.length === 1) {
    const planFileRequest = await fetch(timetableItems[0].urls[0], {
      ...fetchOptions,
      signal: abort
    })

    if (!planFileRequest.ok) {
      throw new Error('could not query timetable plan file')
    }

    const planFileContent = await planFileRequest.arrayBuffer()

    try {
      plan = parseFile(Buffer.from(planFileContent))
    } catch (ex) {
      // ignore
    }
  }

  const filesToHandle = result.files.filter((file) => {
    if (file.urls.length === 1 && weatherUrlRegex.test(file.urls[0])) {
      // the weather file server does not support recent SSL versions
      // which causes trouble for the crawler + it does not work at the
      // vertretungsplan client => just skip it

      return false
    } else {
      return true
    }
  })

  const fileOutput = []

  for (const fileInput of filesToHandle) {
    const urlsWithInfos = []

    for (const url of fileInput.urls) {
      const info = await getUrlInfo(url, abort)

      urlsWithInfos.push({ url, ...info })
    }

    if (urlsWithInfos.length === 0) {
      throw new Error('illegal state')
    }

    fileOutput.push({
      type: fileInput.type,
      mimeType: urlsWithInfos[0].mimeType,
      lastModified: fileInput.lastModified,
      title: fileInput.title,
      file: urlsWithInfos.map((item) => ({
        url: item.url,
        sha512: item.sha512,
        size: item.size
      })),
      id: fileInput.id,
      notify: true
    })
  }

  return {
    file: fileOutput,
    message: result.messages,
    plan
  }
}
