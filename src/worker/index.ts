/*
 * vertretungsplan.io dsb crawler
 * Copyright (C) 2019 - 2022 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Router } from 'express'
import { config, isSchoolConfigComplete } from '../config/index.js'
import { SchoolWorker } from './element.js'

const schoolWorkers = new Map<string, SchoolWorker>()

config.schools.forEach((school) => {
  if (!isSchoolConfigComplete(school)) {
    console.log('skipping ' + school.id + ' due to uncomplete config')
    return
  }

  console.log('init ' + school.id)

  schoolWorkers.set(school.id, new SchoolWorker(school))
})

async function getStatus () {
  const institutions: Array<{
    id: string
    promise: Promise<string>
  }> = []

  schoolWorkers.forEach((worker) => {
    institutions.push({
      id: worker.config.id,
      promise: worker.awaitAllLastPromises()
        .then(() => 'OK')
        .catch(() => 'issues at ' + worker.config.id)
    })
  })

  const resolvedPromises = await Promise.all(institutions.map((item) => item.promise))
  const filteredPromises = resolvedPromises.filter((item) => item !== 'OK')

  if (filteredPromises.length === 0) {
    if (institutions.length === 0) {
      return 'no institutions configured'
    } else {
      return 'OK'
    }
  } else {
    return filteredPromises.join('\n')
  }
}

export const createRouter = () => {
  const router = Router()

  router.get('/vp-status', (_, res) => {
    getStatus()
      .then((status) => res.send(status))
      .catch(() => res.send('failure during getting status'))
  })

  const institutionList: Array<{
    id: string
    title: string
  }> = []

  schoolWorkers.forEach((worker) => {
    institutionList.push({
      id: worker.config.id,
      title: worker.config.title
    })
  })

  router.get('/vp-content', (_, res) => res.json({
    institutions: institutionList
  }))

  schoolWorkers.forEach((worker) => {
    router.use('/vp-institution/' + worker.config.id, worker.createRouter())
  })

  return router
}
