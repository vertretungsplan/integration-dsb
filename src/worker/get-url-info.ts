/*
 * vertretungsplan.io static list integration
 * Copyright (C) 2019 - 2022 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { createHash } from 'crypto'
import LRU from 'lru-cache'
import fetch from 'node-fetch'
import { fetchOptions } from '../request.js'
import { sleep } from '../utils.js'

interface UrlInfo {
  lastModified?: number
  mimeType: string
  sha512: string
  etag?: string
  size: number
}

const cache = new LRU<string, UrlInfo>({
  max: 1000
})

export async function getUrlInfo (url: string, abort: AbortSignal) {
  let i = 1

  for (;;) {
    try {
      const delay = Math.round(Math.random() * 1000)

      await sleep(delay)

      const oldData = cache.get(url)
      const newData = await getUrlInfoInternal(url, abort, oldData)

      cache.set(url, newData)

      return newData
    } catch (ex) {
      if (abort.aborted) throw ex
      if (i++ >= 5) throw ex
    }
  }
}

async function getUrlInfoInternal (url: string, abort: AbortSignal, lastResponse?: UrlInfo): Promise<UrlInfo> {
  const baseHeaders = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0'
  }

  const response = await fetch(url, {
    ...fetchOptions,
    signal: abort,
    headers: lastResponse && lastResponse.etag ? {
      ...baseHeaders,
      'If-None-Match': lastResponse.etag
    } : baseHeaders
  })

  if (response.status === 304) {
    // not modified

    if (!lastResponse) {
      throw new Error('server reported 304 altough there is no lastResponse')
    }

    return lastResponse
  }

  if (response.status !== 200) {
    throw new Error('server reported ' + response.status + ' for ' + url + '; hadLastResponse: ' + (!!lastResponse))
  }

  const etag = response.headers.get('etag')
  const lastModified = Date.parse(response.headers.get('last-modified') || 'invalid')
  const mimeType = response.headers.get('content-type') || 'application/unknown'
  const body = response.body

  const hash = createHash('sha512')
  let size = 0

  // this is because typescript does not understand that this is never null
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  for await (const chunck of body!) {
    hash.update(chunck)

    size += chunck.length
  }

  return {
    sha512: hash.digest('hex'),
    lastModified: isNaN(lastModified) ? undefined : lastModified,
    mimeType,
    etag: etag || undefined,
    size
  }
}
