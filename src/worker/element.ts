/*
 * vertretungsplan.io dsb crawler
 * Copyright (C) 2019 - 2022 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import auth from 'basic-auth'
import { Router } from 'express'
import lodash from 'lodash'
import { SchoolConfiguration } from '../config/schema.js'
import { crawlDsb, CrawlResponse } from './crawl.js'
import { sleep } from '../utils.js'
import timeoutSignal from 'timeout-signal'

const { omit } = lodash

function captitelize (input: string) {
  if (input === '') {
    return input
  } else {
    return input.substring(0, 1).toUpperCase() + input.substring(1)
  }
}

export class SchoolWorker {
  readonly config: SchoolConfiguration
  private dataByCategory = new Map<string, {
    lastPromise: Promise<CrawlResponse>
    lastSuccessPromise: Promise<CrawlResponse>
  }>()

  constructor (config: SchoolConfiguration) {
    this.config = config

    if (config.categories.length !== 2 && config.categories.length !== 1) {
      throw new Error('only 2 or 1 categories supported, got ' + config.categories.length)
    }

    this.initWorkers()
  }

  private initWorkers () {
    this.config.categories.forEach(async (category) => {
      async function crawl () {
        const abort = timeoutSignal(1000 * 60 * 10)

        return await crawlDsb({ ...category, abort })
      }

      const firstCrawl = sleep(Math.random() * 1000 * 60 /* wait up to 60 seconds */)
        .then(() => crawl())

      firstCrawl.catch((ex) => {
        console.warn('initial query failed', ex)
      })

      const promiseData = {
        lastPromise: firstCrawl,
        lastSuccessPromise: firstCrawl
      }

      this.dataByCategory.set(category.id, promiseData)

      try {
        await firstCrawl
      } catch (ex) {
        // ignore
      }

      for (;;) {
        await sleep(1000 * 60 * 5 /* 5 minutes */)

        try {
          const newResponse = await crawl()

          promiseData.lastPromise = Promise.resolve(newResponse)
          promiseData.lastSuccessPromise = Promise.resolve(newResponse)
        } catch (ex) {
          console.warn('crawling failed', ex)

          promiseData.lastPromise = Promise.reject(ex)

          // required to prevent an unhandled Promise rejection
          promiseData.lastPromise.catch(() => null)
        }
      }
    })
  }

  async awaitAllLastPromises () {
    const promises: Array<Promise<unknown>> = []

    this.dataByCategory.forEach((item) => {
      promises.push(item.lastPromise)
    })

    await Promise.all(promises)
  }

  createRouter () {
    const router = Router()

    router.get('/config/default', (_, res) => {
      res.json({
        config: [
          ...this.config.categories.map((category) => ({
            param: this.config.userTypeField,
            type: 'radio',
            value: category.id,
            label: category.label,
            visibilityConditionId: '_true'
          })),
          ...this.config.categories.map((category) => ({
            param: category.passwordField,
            type: 'password',
            visibilityConditionId: 'is' + captitelize(category.id),
            value: '',
            label: category.label + '-Passwort'
          }))
        ],
        configValidationConditionId: 'hasValidUserType',
        contentBucketSets: [
          ...this.config.categories.map((category) => ({
            id: category.id + '_file',
            passwordParam: category.passwordField,
            usageConditionId: 'is' + captitelize(category.id),
            type: 'content'
          })),
          ...this.config.categories.map((category) => ({
            id: category.id + '_plan',
            passwordParam: category.passwordField,
            usageConditionId: 'is' + captitelize(category.id),
            type: 'plan'
          }))
        ],
        conditionSets: [
          ...this.config.categories.map((category) => ({
            id: 'is' + captitelize(category.id),
            type: 'paramIs',
            left: this.config.userTypeField,
            right: category.id
          })),
          {
            id: 'hasValidUserType',
            type: 'or',
            left: 'is' + captitelize(this.config.categories[0].id),
            right: this.config.categories.length === 1 ? '_false' : 'is' + captitelize(this.config.categories[1].id)
          }
        ]
      })
    })

    this.config.categories.forEach((category) => {
      const data = this.dataByCategory.get(category.id)

      if (!data) {
        throw new Error('illegal state')
      }

      router.get('/content/' + category.id + '_file', (req, res, next) => {
        const authData = auth(req)

        if ((!authData) || authData.pass !== category.password) {
          res.setHeader('WWW-Authenticate', 'Basic realm="Login"')
          res.sendStatus(401)
          return
        }

        data.lastSuccessPromise.then((data) => {
          res.json(omit(data, 'plan'))
        }).catch((ex) => next(ex))
      })

      router.get('/plan/' + category.id + '_plan', (req, res, next) => {
        const authData = auth(req)

        if ((!authData) || authData.pass !== category.password) {
          res.setHeader('WWW-Authenticate', 'Basic realm="Login"')
          res.sendStatus(401)
          return
        }

        data.lastSuccessPromise.then((data) => {
          res.json({
            items: data.plan
          })
        }).catch((ex) => next(ex))
      })
    })

    return router
  }
}
